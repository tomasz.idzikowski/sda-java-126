package io.idziksda.designpatterns.after.builder;

public class Computer {
    private String processor;
    private String motherboard;
    private String ram;


    private Computer(ComputerBuilder computerBuilder){
        this.processor = computerBuilder.processor;
        this.motherboard = computerBuilder.motherboard;
        this.ram = computerBuilder.ram;
    }

    public static class ComputerBuilder {
        private String processor;
        private String motherboard;
        private String ram;

        public ComputerBuilder buildProcessor(String processor){
            this.processor = processor;
            return this;
        }

        public ComputerBuilder buildMotherboard(String motherboard){
            this.motherboard = motherboard;
            return this;
        }

        public ComputerBuilder buildRam(String ram){
            this.ram = ram;
            return this;
        }

        public Computer build(){
            return new Computer(this);
        }
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", motherboard='" + motherboard + '\'' +
                ", ram='" + ram + '\'' +
                '}';
    }
}
