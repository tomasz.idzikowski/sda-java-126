package io.idziksda.designpatterns.after.builder;

public class Main {
    public static void main(String[] args) {
        Computer computer = new Computer.ComputerBuilder()
                .buildProcessor("i7")
                .buildMotherboard("Intel")
                .buildRam("16GB")
                .build();
        System.out.println(computer);
    }
}
