package io.idziksda.designpatterns.after.strategy;

public class UpperCut implements Punch{
    @Override
    public void hit() {
        System.out.println("Uppercut");
    }
}
