package io.idziksda.designpatterns.after.strategy;

public class RightPunch implements Punch{
    @Override
    public void hit() {
        System.out.println("Hitting with right punch");
    }
}
