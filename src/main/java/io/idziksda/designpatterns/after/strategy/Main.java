package io.idziksda.designpatterns.after.strategy;

public class Main {
    public static void main(String[] args) {
        Boxer boxer=new Boxer("Rocky");
        boxer.setPunch(new LeftPunch());
        boxer.hitOpponent();
        boxer.setPunch(new RightPunch());
        boxer.hitOpponent();
    }
}
