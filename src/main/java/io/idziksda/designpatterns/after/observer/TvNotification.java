package io.idziksda.designpatterns.after.observer;

public class TvNotification implements Observer{
    @Override
    public void update(Player player) {
        System.out.println("TV: "
                + player.getName() + " zmienil status na "
                + player.getStatus());
    }
}
