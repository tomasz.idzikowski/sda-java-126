package io.idziksda.designpatterns.after.observer;

public interface Observer {
    void update(Player player);
}
