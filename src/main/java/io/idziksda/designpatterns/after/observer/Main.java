package io.idziksda.designpatterns.after.observer;


public class Main {
    public static void main(String[] args) {
        Player player = new Player("Lewy", Status.IDLE);
        System.out.println("Kick off");
        System.out.println(player);
        TvNotification tv = new TvNotification();
        RadioNotification radio = new RadioNotification();
        player.addObserver(tv);
        player.addObserver(radio);

        player.update(Status.TACKLED);
        player.update(Status.PASSED);
        player.update(Status.SCORED);
        player.update(Status.RED_CARD);
    }
}
