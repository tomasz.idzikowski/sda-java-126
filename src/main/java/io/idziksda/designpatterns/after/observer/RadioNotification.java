package io.idziksda.designpatterns.after.observer;

public class RadioNotification implements Observer{
    @Override
    public void update(Player player) {
        System.out.println("Radio: " + player.getName()
                + " własnie jest teraz w stanie "
                + player.getStatus());
    }
}
