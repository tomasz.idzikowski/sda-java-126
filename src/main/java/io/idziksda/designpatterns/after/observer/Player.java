package io.idziksda.designpatterns.after.observer;

import java.util.ArrayList;
import java.util.List;

public class Player implements Observable {
    private String name;
    private Status status;
    private List<Observer> observerList = new ArrayList<>();

    public Player(String name, Status status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyAllObservers() {
        for (Observer observer : observerList) {
         observer.update(this);
        }
    }

    public void update(Status status){
        setStatus(status);
        notifyAllObservers();
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", status=" + status +
                ", observerList=" + observerList +
                '}';
    }
}
