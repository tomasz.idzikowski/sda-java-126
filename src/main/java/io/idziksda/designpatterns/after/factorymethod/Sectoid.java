package io.idziksda.designpatterns.after.factorymethod;

public class Sectoid extends Alien {

    public Sectoid(String rank, int stamina) {
        super(rank, stamina);
    }

    @Override
    public String toString() {
        return "Sectoid{" +
                "rank='" + rank + '\'' +
                ", stamina=" + stamina +
                '}';
    }
}
