package io.idziksda.designpatterns.after.factorymethod;

public abstract class UnitFactory {
    abstract public Alien create(String type);
}
