package io.idziksda.designpatterns.after.factorymethod;

public abstract class Alien {
    protected String rank;
    protected int stamina;

    public Alien(String rank, int stamina) {
        this.rank = rank;
        this.stamina = stamina;
    }

    @Override
    public String toString() {
        return "Alien{" +
                "rank='" + rank + '\'' +
                ", stamina=" + stamina +
                '}';
    }
}
