package io.idziksda.designpatterns.after.factorymethod;

public class Main {
    public static void main(String[] args) {
        UnitFactory factory=new AlienFactory();
        Alien sectoid = factory.create("sectoid");
        Alien muton = factory.create("muton");
        System.out.println(sectoid);
        System.out.println(muton);
    }
}
