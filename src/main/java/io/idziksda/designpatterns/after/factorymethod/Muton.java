package io.idziksda.designpatterns.after.factorymethod;

public class Muton extends Alien {

    public Muton(String rank, int stamina) {
        super(rank, stamina);
    }

    @Override
    public String toString() {
        return "Muton{" +
                "rank='" + rank + '\'' +
                ", stamina=" + stamina +
                '}';
    }
}
