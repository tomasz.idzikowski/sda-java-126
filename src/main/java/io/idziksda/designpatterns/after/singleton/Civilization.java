package io.idziksda.designpatterns.after.singleton;

public class Civilization {
    private static Civilization game;


    private Civilization() {
        System.out.println("constructor");
    }

    public void runGame() {
        System.out.println("Game loaded");
    }

    synchronized public static Civilization getGame() {
        synchronized (Civilization.class) {
            if (game == null) {
                game = new Civilization();
            }
        }
        return game;
    }

    protected Object readResolve(){
        return getGame();
    }
}
