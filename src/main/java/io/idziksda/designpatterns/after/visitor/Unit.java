package io.idziksda.designpatterns.after.visitor;

public interface Unit {
    int invite(Visitor visitor);
}
