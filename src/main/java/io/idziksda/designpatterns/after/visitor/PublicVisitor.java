package io.idziksda.designpatterns.after.visitor;

public class PublicVisitor implements Visitor {
    private String name;

    public PublicVisitor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int visit(F16 f16) {
        if (f16.getWeapon().equals("Fusion Ball")) {
            return 50;
        }
        return 30;
    }

    @Override
    public int visit(Soldier soldier) {
        if (soldier.getName().equals("Antoni")) {
            return 5;
        }
        return 10;
    }

    @Override
    public int visit(Tank tank) {
        if (tank.getName().equals("Rudy-102")) {
            return 20;
        }
        return 10;
    }
}
