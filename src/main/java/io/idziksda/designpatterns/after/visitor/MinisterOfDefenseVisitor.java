package io.idziksda.designpatterns.after.visitor;

public class MinisterOfDefenseVisitor implements Visitor {
    private String name;

    public MinisterOfDefenseVisitor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int visit(F16 f16) {
        if (f16.getWeapon().equals("Heavy Plasma")) {
            return f16.getArmor()+30;
        }
        return f16.getArmor()+50;
    }

    @Override
    public int visit(Soldier soldier) {
        if (soldier.getWeapon().equals("Pistol")) {
            return soldier.getHealth()*2;
        }
        return soldier.getHealth()*5;
    }

    @Override
    public int visit(Tank tank) {
        if (tank.getWeapon().equals("Heavy Plasma")) {
            return tank.getArmour()*3;
        }
        return tank.getArmour()*4;
    }
}
