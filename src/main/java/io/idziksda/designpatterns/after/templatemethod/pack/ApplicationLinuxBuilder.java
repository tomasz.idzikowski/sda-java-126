package io.idziksda.designpatterns.after.templatemethod.pack;

public class ApplicationLinuxBuilder extends ApplicationBuilder {
    @Override
    protected void runIDE() {
        System.out.println("./idea/run.sh");
    }

    @Override
    protected void installIDE() {
        System.out.println("apt-get install idea");
    }
}
