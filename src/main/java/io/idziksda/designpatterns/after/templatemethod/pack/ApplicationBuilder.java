package io.idziksda.designpatterns.after.templatemethod.pack;

public abstract class ApplicationBuilder {
    public void writeApp(){
        openSystem();
        installIDE();
        runIDE();
    }

    protected abstract void runIDE();

    protected abstract void installIDE();

    private void openSystem() {
        System.out.println("Your OS is starting");
    }
}
