package io.idziksda.designpatterns.after.templatemethod.pack;

public class ApplicationWindowsBuilder extends ApplicationBuilder{
    @Override
    protected void runIDE() {
        System.out.println("Double click on shortcut");
    }

    @Override
    protected void installIDE() {
        System.out.println("Switch to control panel and install IDE. Restarting");
    }
}
