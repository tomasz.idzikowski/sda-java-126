package io.idziksda.designpatterns.after.templatemethod;

import io.idziksda.designpatterns.after.templatemethod.pack.ApplicationLinuxBuilder;
import io.idziksda.designpatterns.after.templatemethod.pack.ApplicationWindowsBuilder;

public class Main {
    public static void main(String[] args) {
        ApplicationWindowsBuilder windowsBuilder = new ApplicationWindowsBuilder();
        windowsBuilder.writeApp();
        ApplicationLinuxBuilder linuxBuilder = new ApplicationLinuxBuilder();
        linuxBuilder.writeApp();
    }
}
