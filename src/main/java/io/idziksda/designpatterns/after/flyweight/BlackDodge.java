package io.idziksda.designpatterns.after.flyweight;

public class BlackDodge extends Car {
    public BlackDodge(int mileage) {
        super(mileage, "black");
    }
}
