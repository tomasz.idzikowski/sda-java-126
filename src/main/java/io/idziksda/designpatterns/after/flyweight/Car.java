package io.idziksda.designpatterns.after.flyweight;

public class Car {
    private int mileage;
    private Color color;

    public Car(int mileage, String color) {
        this.mileage = mileage;
        switch (color) {
            case "black":
                this.color = ColorValues.getBlack();
                break;
            default:
                this.color = ColorValues.getWhite();
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "mileage=" + mileage +
                ", color=" + color +
                '}';
    }
}
