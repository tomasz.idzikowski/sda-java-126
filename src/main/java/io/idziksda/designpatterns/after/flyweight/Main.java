package io.idziksda.designpatterns.after.flyweight;

public class Main {
    public static void main(String[] args) {
        BlackDodge blackDodge = new BlackDodge(1000);
        System.out.println(blackDodge);
        WhiteTesla whiteTesla = new WhiteTesla(200);
        System.out.println(whiteTesla);
    }
}
