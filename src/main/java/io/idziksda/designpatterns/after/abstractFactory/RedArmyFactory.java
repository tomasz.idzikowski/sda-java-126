package io.idziksda.designpatterns.after.abstractFactory;

public class RedArmyFactory extends UnitFactory {
    @Override
    public Alien createAlien(String type) {
        switch (type) {
            case "sectoid":
                return new Sectoid("leader", 150);
            case "muton":
                return new Muton("soldier", 100);
            default:
                return new Sectoid("soldier",100);
        }
    }

    @Override
    public Android createAndroid(String type) {
        switch (type) {
            case "T100":
                return new Terminator(100, 1500);
            case "T800":
                return new Terminator(800, 2000);
            default:
                return new Terminator(100,1500);
        }
    }
}
