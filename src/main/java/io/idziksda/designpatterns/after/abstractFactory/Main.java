package io.idziksda.designpatterns.after.abstractFactory;

public class Main {
    public static void main(String[] args) {
        UnitFactory redArmyFactory = new RedArmyFactory();
        UnitFactory blueArmyFactory = new BlueArmyFactory();

        System.out.println(blueArmyFactory.createAndroid("T100"));
        System.out.println(blueArmyFactory.createAlien("sectoid"));

        System.out.println(redArmyFactory.createAndroid("T800"));
        System.out.println(redArmyFactory.createAlien("muton"));
    }
}
