package io.idziksda.designpatterns.after.abstractFactory;

public class BlueArmyFactory extends UnitFactory{
    @Override
    public Alien createAlien(String type) {
        switch (type) {
            case "sectoid":
                return new Sectoid("leader", 200);
            case "muton":
                return new Muton("soldier", 50);
            default:
                return new Sectoid("soldier",50);
        }
    }

    @Override
    public Android createAndroid(String type) {
        switch (type) {
            case "T100":
                return new Terminator(100, 2000);
            case "T800":
                return new Terminator(800, 2500);
            default:
                return new Terminator(100,2000);
        }
    }
}
