package io.idziksda.designpatterns.after.abstractFactory;

public class Terminator extends Android {
    public Terminator(int version, int stamina) {
        super(version, stamina);
    }
}
