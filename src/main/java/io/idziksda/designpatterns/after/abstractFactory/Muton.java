package io.idziksda.designpatterns.after.abstractFactory;

public class Muton extends Alien {
    Muton(String rank, int stamina) {
        super(rank, stamina);
    }
}