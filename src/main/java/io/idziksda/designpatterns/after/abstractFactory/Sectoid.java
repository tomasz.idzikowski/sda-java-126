package io.idziksda.designpatterns.after.abstractFactory;

public class Sectoid extends Alien {
    Sectoid(String rank, int stamina) {
        super(rank, stamina);
    }
}
