package io.idziksda.designpatterns.before.factorymethod;

public class Alien {
    private String rank;
    private int stamina;

    public Alien(String rank, int stamina) {
        this.rank = rank;
        this.stamina = stamina;
    }
}
