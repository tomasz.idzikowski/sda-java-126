package io.idziksda.designpatterns.before.factorymethod;

public class Sectoid extends Alien{

    public Sectoid(String rank, int stamina) {
        super(rank, stamina);
    }
}
