package io.idziksda.designpatterns.before.builder;


public class Computer {
    private String processor;
    private String motherboard;
    private String ram;
    private String graphic;

    public Computer(String processor, String motherboard, String ram, String graphic) {
        this.processor = processor;
        this.motherboard = motherboard;
        this.ram = ram;
        this.graphic = graphic;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor='" + processor + '\'' +
                ", motherboard='" + motherboard + '\'' +
                ", ram='" + ram + '\'' +
                ", graphic='" + graphic + '\'' +
                '}';
    }
}
