package io.idziksda.designpatterns.before.builder;

public class Main {
    public static void main(String[] args) {
        Computer computer=new Computer("Intel i7","Intel","16GB","Radeon");
        System.out.println(computer);
    }
}
