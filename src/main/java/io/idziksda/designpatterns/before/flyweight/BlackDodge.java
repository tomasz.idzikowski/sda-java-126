package io.idziksda.designpatterns.before.flyweight;

public class BlackDodge {
    private int mileage;
    private int red;
    private int green;
    private int blue;

    public BlackDodge(int mileage, int red, int green, int blue) {
        this.mileage = mileage;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    @Override
    public String toString() {
        return "BlackDodge{" +
                "mileage=" + mileage +
                ", red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                '}';
    }
}
