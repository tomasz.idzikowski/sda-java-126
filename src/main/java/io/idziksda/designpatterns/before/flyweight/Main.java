package io.idziksda.designpatterns.before.flyweight;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        BlackDodge blackDodge = new BlackDodge(20000, 0, 0, 0);
        System.out.println(blackDodge);

        List<BlackDodge> list = new ArrayList<>();
        for (int i = 0; i <1000 ; i++) {
            list.add(new BlackDodge(1000,0,0,0));
        }
        list.forEach(car-> System.out.println(car));
    }
}
