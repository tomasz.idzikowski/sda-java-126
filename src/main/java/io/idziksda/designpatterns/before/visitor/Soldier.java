package io.idziksda.designpatterns.before.visitor;

public class Soldier {
    private String name;
    private int health;
    private String weapon;

    public Soldier(String name, int health, String weapon) {
        this.name = name;
        this.health = health;
        this.weapon = weapon;
    }

    public String getWeapon() {
        return weapon;
    }

    public int getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public int getPower(){
        if (getWeapon().equals("Pistol")) {
            return getHealth()*5;
        }
        return getHealth()*2;
    }
}
