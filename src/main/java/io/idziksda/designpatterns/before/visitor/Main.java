package io.idziksda.designpatterns.before.visitor;

public class Main {
    public static void main(String[] args) {
        Soldier soldier = new Soldier("Terminator", 50, "Laser Pistol");
        Tank tank = new Tank("Rudy-102", 300, "Plasma Beam");
        F16 f16 = new F16(250, "Fusion Ball");

        System.out.println(soldier.getPower());
        System.out.println(tank.getPower());
        System.out.println(f16.getPower());
    }
}

