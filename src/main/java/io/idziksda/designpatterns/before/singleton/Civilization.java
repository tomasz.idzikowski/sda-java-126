package io.idziksda.designpatterns.before.singleton;

public class Civilization {
    private int MAX_UNITS=100;

    public Civilization() {
        System.out.println("constructor");
    }

    public void runGame(){
        System.out.println("Game loaded");
    }
}
