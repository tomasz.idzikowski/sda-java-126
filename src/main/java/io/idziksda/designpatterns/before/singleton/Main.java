package io.idziksda.designpatterns.before.singleton;

public class Main {
    public static void main(String[] args) {
        Civilization game1 = new Civilization();
        Civilization game2 = new Civilization();
        System.out.println(game1==game2);
    }
}
