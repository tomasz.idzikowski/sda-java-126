package io.idziksda.designpatterns.before.observer;

public class TvNotification {
    public void updatePlayer(Player player) {
        System.out.println("TV: "
                + player.getName() + " zmienil status na "
                + player.getStatus());
    }
}
