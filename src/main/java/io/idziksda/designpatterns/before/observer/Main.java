package io.idziksda.designpatterns.before.observer;

public class Main {
    public static void main(String[] args) {
        Player player = new Player("Lewy", Status.IDLE);
        System.out.println("Kick off");
        System.out.println(player);
        TvNotification tv = new TvNotification();
        RadioNotification radio = new RadioNotification();
        player.setStatus(Status.TACKLED);
        tv.updatePlayer(player);
        radio.updatePlayer(player);
        player.setStatus(Status.PASSED);
        tv.updatePlayer(player);
        radio.updatePlayer(player);
        player.setStatus(Status.SCORED);
        tv.updatePlayer(player);
        radio.updatePlayer(player);
        player.setStatus(Status.TACKLED);
        tv.updatePlayer(player);
        radio.updatePlayer(player);
    }
}
