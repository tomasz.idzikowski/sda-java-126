package io.idziksda.designpatterns.before.observer;

public class RadioNotification {
    public void updatePlayer(Player player) {
        System.out.println("Radio: " + player.getName()
                + " własnie jest teraz w stanie "
                + player.getStatus());
    }
}
