package io.idziksda.designpatterns.before.strategy;

public class Main {
    public static void main(String[] args) {
        Boxer rocky = new Boxer("Rocky");
        System.out.println("Jestem  " + rocky.getName());
        rocky.leftPunch();
        rocky.rightPunch();
        rocky.leftPunch();
        rocky.leftPunch();
        rocky.upperCut();
    }
}
