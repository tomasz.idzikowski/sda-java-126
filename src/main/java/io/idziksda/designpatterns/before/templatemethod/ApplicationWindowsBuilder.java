package io.idziksda.designpatterns.before.templatemethod;

public class ApplicationWindowsBuilder {
    public void writeApp() {
        openSystem();
        installIDE();
        restartSystem();
        runIDE();
    }

    private void runIDE() {
        System.out.println("Double click on shortcut");
    }

    private void restartSystem() {
        System.out.println("System restarted");
    }

    private void installIDE() {
        System.out.println("Switch to control panel and install IDE");
    }

    private void openSystem() {
        System.out.println("Your OS is starting");
    }

}
