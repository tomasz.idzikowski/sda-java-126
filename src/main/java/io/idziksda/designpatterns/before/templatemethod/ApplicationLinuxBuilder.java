package io.idziksda.designpatterns.before.templatemethod;

public class ApplicationLinuxBuilder {
    public void writeApp(){
        openSystem();
        installIDE();
        runIDE();
    }

    private void runIDE() {
        System.out.println("./idea/run.sh");
    }

    private void installIDE() {
        System.out.println("apt-get install idea");
    }

    private void openSystem() {
        System.out.println("Your OS is starting");
    }
}
