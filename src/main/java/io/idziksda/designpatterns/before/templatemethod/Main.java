package io.idziksda.designpatterns.before.templatemethod;

public class Main {
    public static void main(String[] args) {
        ApplicationWindowsBuilder windowsBuilder = new ApplicationWindowsBuilder();
        windowsBuilder.writeApp();
    }
}
