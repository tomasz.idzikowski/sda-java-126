package io.idziksda.designpatterns.zadania.flyweight;

public class Destroyer extends Unit{
    public Destroyer(int position) {
        super(position, StatsValues.getDestroyerStats());
    }
}
