package io.idziksda.designpatterns.zadania.flyweight;

public class Main {
    public static void main(String[] args) {
        Unit tank = new Tank(1000);
        System.out.println(tank);
    }
}
