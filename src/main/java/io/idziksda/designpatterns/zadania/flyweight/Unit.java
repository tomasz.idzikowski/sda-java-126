package io.idziksda.designpatterns.zadania.flyweight;

public class Unit {
    private int position;
    private Stats stats;

    public Unit(int position, Stats stats) {
        this.position = position;
        this.stats = stats;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "position=" + position +
                ", stats=" + stats +
                '}';
    }
}
