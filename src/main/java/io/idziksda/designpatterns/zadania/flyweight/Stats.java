package io.idziksda.designpatterns.zadania.flyweight;

public class Stats {
    private int hp;
    private int armour;
    private int speed;
    private int cost;

    public Stats(int hp, int armour, int speed, int cost) {
        this.hp = hp;
        this.armour = armour;
        this.speed = speed;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "hp=" + hp +
                ", armour=" + armour +
                ", speed=" + speed +
                ", cost=" + cost +
                '}';
    }
}
