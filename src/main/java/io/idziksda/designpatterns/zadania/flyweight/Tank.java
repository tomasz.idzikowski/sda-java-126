package io.idziksda.designpatterns.zadania.flyweight;

public class Tank extends Unit{

    public Tank(int position) {
        super(position, StatsValues.getTankStats());
    }
}
