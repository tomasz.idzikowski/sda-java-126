package io.idziksda.designpatterns.zadania.flyweight;

public class RifleMan extends Unit{
    public RifleMan(int position) {
        super(position, StatsValues.getRifleManStats());
    }
}
