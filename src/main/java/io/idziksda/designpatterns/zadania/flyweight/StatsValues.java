package io.idziksda.designpatterns.zadania.flyweight;

public class StatsValues {
    private static final Stats tankStats = new Stats(100, 100, 80, 2000);
    private static final Stats rifleManStats = new Stats(20, 20, 50, 10);
    private static final Stats destroyerStats = new Stats(150, 200, 40, 4000);

    public static Stats getTankStats() {
        return tankStats;
    }

    public static Stats getRifleManStats() {
        return rifleManStats;
    }

    public static Stats getDestroyerStats() {
        return destroyerStats;
    }


}
